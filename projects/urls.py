from django.urls import path
from .views import show_list, show_tasks, create_project

urlpatterns = [
    path("", show_list, name="list_projects"),
    path("<int:id>/", show_tasks, name="show_project"),
    path("create/", create_project, name="create_project"),
]
