from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from .forms import ProjectForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def show_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


def redirect_to_page(request):
    return redirect("list_projects")


@login_required
def show_tasks(request, id):
    projects = get_object_or_404(Project, tasks=id)
    tasks = Task.objects.filter(project=id)
    context = {
        "projects": projects,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.purchaser = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
